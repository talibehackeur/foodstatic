<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 16:05
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Produit;
use Nelmio\Alice\ProcessorInterface;

class ProduitProcessor implements ProcessorInterface
{

    /**
     * Processes an object before it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function preProcess($object)
    {
        if ($object instanceof Produit) {
            if ($object->isSponsered())
                $object->setSponsorisedDate(new \DateTime());
        }
    }

    /**
     * Processes an object after it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function postProcess($object)
    {
        // TODO: Implement postProcess() method.
    }
}