<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 14:04
 */

namespace AppBundle\DataFixtures\ORM;

class CategorieProvider
{
    /**
     * @param integer $indice
     * @return string
     */
    public static function categorieName($indice)
    {
        $categories = [
            'Informatique',
            'Téléphonie',
            'Déco, linge Maison',
            'Vêtements',
            'Sport & Hobbies',
            'Jeux & Jouets',
            'Consoles & Jeux vidéos',
            'Electromenager',
            'Mobilier',
            'Cosmétique',
            'Parfumerie',
            'Montre',
            'Bijoux',
            'Livre, CD, DVD',
            'Véhicules',
        ];
        return $categories[$indice-1];
    }
}