<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 17:49
 */

namespace AppBundle\DataFixtures\ORM;


class MediaProvider
{
    public static function profile($current)
    {
        return '@mediasProfile'.$current;
    }
    public static function couverture($current)
    {
        return '@mediasCouverture'.$current;
    }
    public static function produit($current)
    {
        return '@mediasProduit'.$current;
    }
    public static function categorie($current)
    {
        return '@mediasCategorie'.$current;
    }

}