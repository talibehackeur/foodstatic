<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 15:46
 */

namespace AppBundle\DataFixtures\ORM;



use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;

class DataLoader extends AbstractLoader
{
    /**
     * {@inheritdoc}
     */
    public function getFixtures()
    {
        return [
            __DIR__.'/medias.yml',
            __DIR__.'/categories.yml',
            __DIR__.'/produits.yml',
        ];
    }

}