<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 16:03
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Media;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic;
use Nelmio\Alice\ProcessorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageProcessor implements ProcessorInterface
{
    /**
     * @var Filesystem
     */
    private $fileSystem;


    private $sourceDir = __DIR__ . '/../../../../web/';
    private $destinationDir = __DIR__ . '/../../../../web/';

    /**
     * ImageProcessor constructor.
     * @internal param Filesystem $fileSystem
     */
    public function __construct()
    {
        $this->fileSystem = new Filesystem();
         /*$this->fileSystem->remove(array('file',$this->destinationDir.'medias/pp','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/pp_fit','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/pc','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/pc_fit','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits_fit','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits_s_m','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits_s_l','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits_s_xl','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/produits_s_xxl','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/categories','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/categories_fit','*.*'));
         $this->fileSystem->remove(array('file',$this->destinationDir.'medias/categories_detail','*.*'));*/
    }

    /**
     * Processes an object before it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function postProcess($object)
    {
    }

    /**
     * Processes an object after it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function preProcess($object)
    {
        if ($object instanceof Media) {
            switch ($object->getAlt()) {
                case 'categorie':
                    $file = new UploadedFile($this->sourceDir .'images/categorie.jpg',$object->getAlt());
                    $object->setFile($file);
                    break;
                case 'produit':
                    $file = new UploadedFile($this->sourceDir .'images/produits/' . rand(1, 3) . '.jpg',$object->getAlt());
                    $object->setFile($file);
                    break;
                case 'couverture':
                    $file = new UploadedFile($this->sourceDir .'images/pc.jpg',$object->getAlt());
                    $object->setFile($file);
                    break;
                case 'profile':
                    $file = new UploadedFile($this->sourceDir .'images/pp.jpg',$object->getAlt());
                    $object->setFile($file);
                    break;
            }
        }
    }
}