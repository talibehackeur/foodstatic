<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 14:04
 */

namespace AppBundle\DataFixtures\ORM;

class ProduitProvider
{
    /**
     * @return string
     * @internal param int $indice
     */
    public static function produitName()
    {
        $prduits = [
            'GALANT caisson à dossiers suspendus                 ',
            'GALANT combinaison de rangement ouverte             ',
            'GALANT combinaison rangements av dossiers           ',
            'GALANT combinaison rangements av dossiers           ',
            'GALANT combinaison rangements av dossiers           ',
            'GALANT combinaison rgt ptes coulissantes            ',
            'GALANT série de rangement                           ',
            'GALEJ photophore                                    ',
            'GAMLEBY chaise                                      ',
            'GAMLEBY étagère murale                              ',
            'GAMLEBY table à rabat                               ',
            'GRUA miroir                                         ',
            'GRUNDTAL patère pour porte                          ',
            'GRUNDTAL série buanderie                            ',
            'GRUNDTAL série cuisine                              ',
            'GRUNDTAL série de salle de bain                     ',
            'GRUNDVATTNET série                                  ',
            'GRUNKA ustensiles, 4 pièces                         ',
            'GRUSBLAD couette, chaude                            ',
            'GRUSBLAD couette, légère                            ',
            'GRUSBLAD couette toutes saisons                     ',
            'GRYNET panneau                                      ',
            'GUBBRÖRA pinceau à pâtisserie                       ',
            'GUBBRÖRA pinceau à pâtisserie                       ',
            'GUBBRÖRA pinceau à pâtisserie                       ',
            'GUBBRÖRA pinceau à pâtisserie                       ',
            'GUBBRÖRA spatule en caoutchouc                      ',
            'GULLVIVA housse de coussin                          ',
            'GULSPORRE rideaux, 1 paire                          ',
            'GULTOPPA plaid                                      ',
            'GUNDE chaise pliante                                ',
            'GUNDE chaise pliante                                ',
            'GUNGGUNG balançoire                                 ',
            'GUNNABO série                                       ',
            'GUNNERN armoire verrouillable                       ',
            'GUNNERN armoire verrouillable                       ',
            'GUNNERN guéridon                                    ',
            'GUNNERN meuble à miroir 1 porte                     ',
            'GUNNI rideaux occultant, 1 paire                    ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'GURLI housse de coussin                             ',
            'PLANERA verre                                       ',
            'PLASTIS bac à glaçons                               ',
            'PLASTIS brosse à vaisselle                          ',
            'PLATS salière / poivrière, lot de 2                 ',
            'PLATTBOJ pile lithium                               ',
            'PLATYCODON plante en pot                            ',
            'PLAYFULNESS collection                              ',
            'PLISTER housse de couette et 2 taies                ',
            'PLUFSIG tapis de gymnastique pliant                 ',
            'PLUGGIS poubelle de tri                             ',
            'PLUGGIS série                                       ',
            'PLURING housse vêtements lot de 3                   ',
            'PLUSSIG lavette                                     ',
            'POÄNG coussin fauteuil                              ',
            'POÄNG coussin repose - pieds                        ',
            'POÄNG série                                         ',
            'POFFARE horloge murale                              ',
            'POKAL série                                         ',
            'POLARVIDE plaid                                     ',
            'POLARVIDE plaid                                     ',
            'POLARVIDE plaid                                     ',
            'POLARVIDE plaid                                     ',
            'POLARVIDE plaid                                     ',
            'POMP vase                                           ',
            'lanterneVIFT bougie bloc parf                       ',
            'VIGDIS housse de coussin                            ',
            'VIGDIS housse de coussin                            ',
            'VIGDIS housse de coussin                            ',
            'VIGDIS housse de coussin                            ',
            'VIGDIS housse de coussin                            ',
            'VIGDIS housse de coussin                            ',
            'VIKARE barrière lit                                 ',
            'VIKIS réveil                                        ',
            'VIKTIGT collection                                  ',
            'VIKT lampe murale à LED                             ',
            'VILASUND convertible 2 places                       ',
            'VILASUND convertible 3 places                       ',
            'VILASUND convertible avec méridienne                ',
            'VILASUND housse de convertible / méridienne         ',
            'VILASUND housse de convertible 2places              ',
            'VILASUND housse de convertible 3places              ',
            'VILBORG rideaux, 1 paire                            ',
            'VILBORG rideaux, 1 paire                            ',
            'VILDAPEL cache - pot                                ',
            'VILDAPEL cache - pot                                ',
            'VILDAPEL cache - pot                                ',
            'VILDAPEL série                                      ',
            'VILLIG bougie parfumée à 3 mèches                   ',
            'VILLSTAD fauteuil                                   ',
            'VILLSTAD fauteuil haut                              ',
            'VILLSTAD repose - pieds                             ',
            'VILMAR chaise                                       ',
            'VILMAR chaise                                       ',
            'VILMAR chaise                                       ',
            'VILMAR chaise '

        ];
        return $prduits[array_rand($prduits)];
    }
}