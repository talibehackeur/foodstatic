<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Produit
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Produit
{
    /**
     * @var int
     * @Serializer\Expose()
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Serializer\Expose()
     * @Assert\NotBlank(message="Ce champ ne doit pas etre vide")
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var float
     * @Serializer\Expose()
     * @Assert\NotBlank(message="Ce champ ne doit pas etre vide")
     * @Assert\Range(min=0, minMessage="Ce nombre doit être positif")
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;


    /**
     * @var integer
     * @Serializer\Expose()
     * @Assert\NotBlank(message="Ce champ ne doit pas etre vide")
     * @Assert\Range(min=0, minMessage="Ce nombre doit être positif")
     * @ORM\Column(name="quantite", type="integer")
     */
    private $quantite;
    /**
     * @var string
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @var float
     * @ORM\Column(name="note", type="float", nullable=true)
     */
    private $note=0;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Media",cascade={"persist","refresh","remove"})
     * @Serializer\Expose()
     * @ORM\JoinColumn(nullable=true)
     */
    private $media;

    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime",name="created_at")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime",name="updated_at")
     */
    private $updatedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }


    /**
     * @Assert\NotBlank(message="Vous devez choisir une catégorie")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categorie",inversedBy="produits")
     * @ORM\JoinColumn(nullable=true)
     */
    private $categorie;
    /**
     * @Assert\NotBlank(message="Ce champ ne doit pas etre vide")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays",inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pays;
    /**
     * @Assert\NotBlank(message="Ce champ ne doit pas etre vide")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Producer",inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $producer;
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile($file)
    {
        if ($file) {
            $this->file = $file;
            $media = new Media();
            $media->setDestination('produits')
                ->setFile($file);
            if ($media->isValidImage())
                $this->setMedia($media);
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Produit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Produit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set media
     *
     * @param Media $media
     *
     * @return Produit
     */
    public function setMedia(Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return Media
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\Categorie $categorie
     *
     * @return Produit
     */
    public function setCategorie(\AppBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }



    /**
     * Set quantite
     *
     * @param integer $quantite
     *
     * @return Produit
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Get quantite
     *
     * @return integer
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set pays
     *
     * @param \AppBundle\Entity\Pays $pays
     *
     * @return Produit
     */
    public function setPays(\AppBundle\Entity\Pays $pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set note
     *
     * @param float $note
     *
     * @return Produit
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return float
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set producer
     *
     * @param \AppBundle\Entity\Producer $producer
     *
     * @return Produit
     */
    public function setProducer(\AppBundle\Entity\Producer $producer)
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer
     *
     * @return \AppBundle\Entity\Producer
     */
    public function getProducer()
    {
        return $this->producer;
    }
}
