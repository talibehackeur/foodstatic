<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Intervention\Image\ImageManagerStatic;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Media
 *
 * @ORM\Table(name="media")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 */
class Media
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var string
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;
    /**
     * @var string
     * @ORM\Column(name="alt", type="string", length=255)
     */
    private $alt;

    /**
     * @var UploadedFile
     */
    private $file;
    /**
     * @var string
     */
    /**
     * @var string
     * @ORM\Column(name="destination", type="string", length=255)
     */
    private $destination;

    /**
     * @var string
     */
    private $tempFileName;
    /**
     * @var string
     */
    private $idImage;
    /**
     * @var string
     */
    private $urlImage;
    /**
     * @var \DateTime $createdAt
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime",name="created_at")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime",name="updated_at")
     */
    private $updatedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Set destination
     *
     * @param string $destination
     * @return Media
     */
    public function setDestination($destination)
    {
        $this->destination = $destination . DIRECTORY_SEPARATOR;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * @param string $alt
     * @return $this
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     * @return $this
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTempFileName()
    {
        return $this->tempFileName;
    }

    /**
     * @param mixed $tempFileName
     * @return $this
     */
    public function setTempFileName($tempFileName)
    {
        $this->tempFileName = $tempFileName;
        return $this;
    }


    /**
     * @ORM\PreRemove()
     */
    public function preRemove()
    {
        $this->idImage = $this->getId();
        $this->urlImage = $this->getUrl();
        $this->tempFileName = $this->getUploadRootDir() . $this->getDestination() . $this->getId() . '.' . $this->getUrl();
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @ORM\PostRemove()
     */
    public function remove()
    {
        if (file_exists($this->tempFileName)) {
            try {
                unlink($this->getUploadRootDir() . 'produits' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_fit' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_s_l' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_s_m' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_s_xl' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_s_xxl' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
                unlink($this->getUploadRootDir() . 'produits_quickview' . DIRECTORY_SEPARATOR . $this->idImage . '.' . $this->urlImage);
            } catch (\Exception $e) {

            }

        }
    }

    public function getUploadRootDir()
    {
        return dirname(dirname(dirname(__DIR__))) . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . 'medias' . DIRECTORY_SEPARATOR;
    }


    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if ($this->file === null) {
            return;
        }
        // au cas ou les deux fichiers n'ont pas la meme extension car yaura pas de remplacemnt
        $this->tempFileName = $this->getUploadRootDir() . $this->getDestination() . $this->getId() . '.' . $this->getUrl();
        $this->setUrl($this->file->guessExtension());
        $this->setAlt($this->file->getClientOriginalName());
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        // Si jamais il n'y a pas de fichier (champ facultatif)
        if (null === $this->file) {
            return;
        }
        if (file_exists($this->tempFileName) && !is_dir($this->tempFileName)) {
            unlink($this->tempFileName);
        }
        ImageManagerStatic::make($this->file)
            ->save($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl());
        if ($this->destination === "pc/") {
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(1200, 350, function ($constraint) {
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'pc_fit' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
        } elseif ($this->destination === "pp/") {
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(200, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'pp_fit' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
        } elseif ($this->destination === "produits/") {
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(285, 380, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_fit' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(490, 490, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_s_xxl' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(490, 245, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_s_xl' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(300, 491, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_s_l' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(600, 600, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_quickview' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(241, 241, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'produits_s_m' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
        } elseif ($this->destination === "categories/") {
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(452, 228, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'categories_fit' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());
            ImageManagerStatic::make($this->getUploadRootDir() . $this->destination . $this->getId() . '.' . $this->getUrl())
                ->fit(850, 300, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upSize();
                })
                ->save($this->getUploadRootDir() . 'categories_detail' . DIRECTORY_SEPARATOR . $this->getId() . '.' . $this->getUrl());

        }

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    public function isValidImage()
    {
        if (!$this->file)
            return false;
        return (
            $this->file->guessExtension() === 'jpeg'
            || $this->file->guessExtension() === 'gif'
            || $this->file->guessExtension() === 'png'
            || $this->file->guessExtension() === 'x-png'
        );
    }
}
