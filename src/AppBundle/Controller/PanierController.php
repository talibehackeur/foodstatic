<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Produit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PanierController extends BaseController
{
    /**
     * @param Request $request
     * @param $id
     * @return Response
     * @Route("/ajouter-panier/{id}",name="ajouter-au-panier",options={"expose"=true})
     * @Method("POST")
     */
    public function ajouterAction(Request $request, $id)
    {
        $produit = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->withMedia($id);
        if (!$produit)
            return new JsonResponse(array('error' => 'Le produit n\'existe pas'), 404);
        $dansPanier = true;
        $key = (string)$produit->getId();
        $quantites = json_decode($request->getContent(), true);
        $quantite = 1;
        if ($quantites['quantite'] != null) {
            $quantite = $quantites['quantite'];
        }
        $session = $this->get('session');
        if (!$session->has('panier')) {
            $session->set('panier', array());
        }
        $panier = $session->get('panier');
        if (isset($panier[$key])) {
            if ($produit->getQuantite() < ($panier[$key] + $quantite))
                return new JsonResponse(array('error' => 'Stock insuffisant'), 400);
            $panier[$key] += $quantite;
        } else {
            if ($produit->getQuantite() < $quantite)
                return new JsonResponse(array('error' => 'Stock insuffisant'), 400);
            $panier[$key] = $quantite;
            $dansPanier = false;
        }
        $session->set('panier', $panier);
        if (!$dansPanier) {
            $p = $this->get('jms_serializer')->serialize(
                array(
                    'produit' => $produit,
                    'panier' => $panier
                ), 'json'
            );
            return new Response($p, 200, array(
                'Content-Type' => 'application/json'
            ));
        }
        return new JsonResponse(null, 204);
    }

    /**
     * @param Request $request
     * @param Produit $produit
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/dimunier-panier/{id}",name="dimunier-panier",options={"expose"=true})
     * @Method("POST")
     */
    public function dimunierAction(Request $request, Produit $produit)
    {
        if (!$produit)
            return new JsonResponse(array('error' => 'Le produit n\'existe pas'), 404);
        $key = (string)$produit->getId();
        $session = $this->get('session');
        if (!$session->has('panier')) {
            $session->set('panier', array());
        }
        $panier = $session->get('panier');
        if (isset($panier[$key]) && $panier[$key] > 0) {
            $panier[$key]--;
            if ($panier[$key] === 0)
                unset($panier[$key]);
            $session->set('panier', $panier);
            return new JsonResponse(null);
        }
        return new JsonResponse(null, 404);
    }

    /**
     * @param Request $request
     * @param Produit $produit
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/enlever-panier/{id}",name="enlever-dans-panier",options={"expose"=true})
     * @Method("POST")
     */
    public function enleverAction(Request $request, Produit $produit)
    {
        if (!$produit)
            return new JsonResponse(array('error' => 'Le produit n\'existe pas'), 404);
        $key = (string)$produit->getId();
        $session = $this->get('session');
        $panier = $session->get('panier');
        if ($panier && isset($panier[$key])) {
            unset($panier[$key]);
            $session->set('panier', $panier);
            return new JsonResponse(array('panier' => $panier));
        }
        return new JsonResponse(null, 204);

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/vider-panier",name="vider-panier",options={"expose"=true})
     * @Method("GET")
     */
    public function viderAction(Request $request)
    {
        $session = $this->get('session');
        if ($session->has('panier')) {
            $session->remove('panier');
        }
        return new JsonResponse(null, 204);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/get-panier",name="get-panier",options={"expose"=true})
     * @Method("GET")
     */
    public function getTotalPanierAction()
    {
        $total = 0;
        $session = $this->get('session');
        if ($session->has('panier')) {
            $panier = $session->get("panier");
            foreach ($panier as $key => $value) {
                $total += $value;
            }
        }
        return new JsonResponse(array('total' => $total), 200);
    }

    /**
     * @Route("/mon-panier",name="mon-panier",options={"expose"=true})
     */
    public function panierIndexAction()
    {
        return $this->render(':achats:panier.html.twig', array(
            'villes' => $this->getVilles()
        ));
    }

    /**
     * @Route("/produits-dans-panier",name="produits-panier",options={"expose"=true})
     * @Method("GET")
     * @return JsonResponse
     */
    public function produitsDansPanier()
    {
        $panier = $this->get('session')->get('panier');
        if ($panier) {
            $produis = $this->getDoctrine()->getRepository('AppBundle:Produit')
                ->getProduitsDansPanier(array_keys($panier));
            return new JsonResponse(array(
                    'produits' => $produis,
                    'panier' => $panier
                )
            );
        } else {
            return new JsonResponse(array('produits' => null));
        }
    }
}
