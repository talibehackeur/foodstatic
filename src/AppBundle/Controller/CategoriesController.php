<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Categorie;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Produit;
use AppBundle\Form\ProduitType;

/**
 * Produit controller.
 *
 * @Route("/categorie")
 */
class CategoriesController extends BaseController
{
    /**
     * Lists all Produit entities.
     *
     * @Route("/{id}/produits", name="categorie_produit")
     * @Method("GET")
     * @param Categorie $categorie
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function produitsAction(Categorie $categorie, Request $request)
    {
        if (!$categorie)
            $this->createNotFoundException("Cette catégorie n'existe pas ");
        $mode = $request->query->get('mode');
        $qb = $this->getDoctrine()->getRepository('AppBundle:Produit')->getProduitsByCatgeorie($categorie);
        $pager = $this->get('ecommerce.paginator_factory');
        $produits = $pager->getPaginatedCollection($qb, $request);
        return $this->render(':produit:produit_par_categorie.html.twig', array(
            'categorie'=> $categorie,
            'produits' => $produits,
            'pager' => $pager->getPagerfanta(),
            'mode' => $mode
        ));
    }

    /**
     * Lists all categorie entities.
     *
     * @Route("/", name="categories_list")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listCategorieAction(Request $request)
    {
        $qb = $this->getDoctrine()->getRepository('AppBundle:Categorie')->withMedia();
        $pager = $this->get('ecommerce.paginator_factory');
        $categories = $pager->getPaginatedCollection($qb, $request);
        return $this->render('categories/list-categorie.html.twig', array(
            'categories' => $categories,
            'pager' => $pager->getPagerfanta()
        ));
    }


}
