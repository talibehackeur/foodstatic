<?php

namespace AppBundle\Controller;

use Shared\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Produit;
use AppBundle\Form\ProduitType;

/**
 * Produit controller.
 *
 * @Route("/produit")
 */
class ProduitsController extends BaseController
{


    /**
     * Finds and displays a Produit entity.
     *
     * @Route("/{id}", name="produit_show",options={"expose"=true})
     * @Method("GET")
     */
    public function showAction($id)
    {
        $produit = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->withMedia($id);
        if (!$produit)
            $this->createNotFoundException("Ce produit n'existe pas !!");

        $relatedProduct = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->alsoLike($produit->getCategorie());
        return $this->render('produit/show.html.twig', array(
            'p' => $produit,
            'villes' => $this->getVilles(),
            'rp' => $relatedProduct
        ));
    }

    /**
     * Finds and displays a Produit entity.
     *
     * @Route("/{id}/quickview", name="produit_quickview",options={"expose"=true})
     * @Method("GET")
     */
    public function quickViewAction($id)
    {
        $produit = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->withMedia($id);
        if (!$produit)
            $this->createNotFoundException("Ce produit n'existe pas !!");
        return $this->render(':produit:quick_view.html.twig', array(
            'p' => $produit,
        ));
    }


}
