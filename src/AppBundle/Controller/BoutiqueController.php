<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Media;
use AppBundle\Entity\Pays;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use UserBundle\Entity\User;

class BoutiqueController extends BaseController
{

    /**
     * @Route("/produits/{name}",name="show_catalogue")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function catalogueAction(Request $request, $name)
    {
        $pays = $this->getDoctrine()->getRepository("AppBundle:Pays")
            ->findOneBy(array("nom" => $name));
        if (!$pays)
            $this->createNotFoundException("Ce pays n'existe pas dans notre boutique ");
        $mode = $request->query->get('mode');
        $qb = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->produitParPays($pays);
        $pager = $this->get('ecommerce.paginator_factory');
        $produits = $pager->getPaginatedCollection(
            $qb,
            $request
        );
        return $this->render("boutiques/detail-boutique.html.twig", array(
            'produits' => $produits,
            'pays' => $pays,
            'pager' => $pager->getPagerfanta(),
            'mode' => $mode,
            'villes' => $this->getVilles()
        ));
    }

    /**
     * @Route("/client/checkout",name="checkout")
     * @Method("GET")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function checkoutAction(Request $request)
    {
        if (!$this->getUser()->getActif()) {
            $this->addFlash("danger", "Votre compte est temporairement bloquè, veuillez nous contacter pour plus de dètail");
            return $this->redirectToRoute("mon-panier");
        }
        if (!$this->get("session")->has("panier")) {
            $this->addFlash("danger", "Votre panier est vide, parcourez nos produits pour le remplir");
            return $this->redirectToRoute("mon-panier");
        }
        $panier = $this->get("session")->get("panier");

        $produits = $this->getDoctrine()->getRepository("AppBundle:Produit")
            ->findBy(array("id" => array_keys($panier)));
        $totalPrix = 0;
        foreach ($panier as $key => $value) {
            foreach ($produits as $produit) {
                if ($produit->getId() == $key) {
                    $totalPrix += intval($value) * $produit->getPrix();
                    $produit->setQuantite($produit->getQuantite() - intval($value));
                }
            }
        }
        $html = $this->renderView(':pdf:facture.html.twig', array(
            "produits" => $produits,
            "panier" => $panier,
            "total" => $totalPrix,
            "user" => $this->getUser()
        ));
        $html2pdf = $this->get('html2pdf_factory')->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($html);

        //Output envoie le document PDF au navigateur internet avec un nom spécifique qui aura un rapport avec le contenu à convertir (exemple : Facture, Règlement…)
        $name = Media::getUploadRootDirStatic() . "factures" . DIRECTORY_SEPARATOR . $this->getUser()->getPseudo() . uniqid() . '-facture.pdf';
        $html2pdf->Output($name, 'F');
        $this->get("session")->remove("panier");
        $this->getDoctrineManager()->flush();
        $this->addFlash("success", "Votre commande a ete bien prise en compte, vous serez livre dans les plus brefs dèlais");
        $this->get("session")->set("facture", $name);
        return $this->render(":achats:panier.html.twig", array(
            "villes" => $this->getVilles(),
            "dowloadPdf" => true
        ));

    }

    /**
     * @Route("facture/telecharger", name="download_td")
     **/
    public function downloadCoursAction()
    {
        if (!$this->get("session")->has("facture")) {
            $this->addFlash("warning", "Vous n'avez pas de facture recent");
            return $this->redirectToRoute("homepage");
        }
        $response = new BinaryFileResponse($this->get("session")->get("facture"));
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
        $response->headers->set("Content-Type", "application/pdf");
        return $response;
    }
}
