<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Media;
use Intervention\Image\ImageManagerStatic;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="homepage",options={"expose"=true})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $pays = $this->getDoctrine()->getRepository('AppBundle:Pays')
            ->findAll();
        return $this->render('default/index.html.twig', array(
            'pays' => $pays,
            'villes' => $this->getVilles()
        ));
    }
}
