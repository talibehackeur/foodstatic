<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 17/09/2016
 * Time: 16:04
 */

namespace UserBundle\security;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use UserBundle\Entity\User;

class FormLoginAuthenticator extends AbstractFormLoginAuthenticator
{
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var User
     */
    private $user;

    /**
     * FormLoginAuthenticator constructor.
     * @param UserPasswordEncoder $passwordEncoder
     * @param Router $router
     * @param EntityManager $em
     * @param Session $session
     */
    public function __construct(UserPasswordEncoder $passwordEncoder, Router $router, EntityManager $em, Session $session)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->router = $router;
        $this->em = $em;
        $this->session = $session;
    }


    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() !== '/login_check') {
            return;
        }
        $username = $request->request->get('_username');
        $request->getSession()->set(Security::LAST_USERNAME, $username);
        $password = $request->request->get('_password');
        return array(
            'username' => $username,
            'password' => $password
        );
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $userRepo = $this->em
            ->getRepository('UserBundle:User');
        $user = $userRepo->findByUsernameOrEmail($credentials['username']);
        if ($user)
            return $user;
        throw new CustomUserMessageAuthenticationException("Ce nom d'utilisateur n'existe pas");
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $plainPassword = $credentials['password'];
        if (!$this->passwordEncoder->isPasswordValid($user, $plainPassword)) {
            throw new CustomUserMessageAuthenticationException("Mot de passe incorrecte");

        }
        if (!$user->isActif() && $user->getVerificationCode()) {
            $this->session->set('mailToValide', $user->getMail());
            return false;
        }
        $this->user = $user;
        return true;
    }

    protected function getDefaultSuccessRedirectUrl()
    {
        if (in_array('ROLE_ADMIN', $this->user->getRoles(), true))
            return $this->router
                ->generate('admin');
        return $this->router
            ->generate('homepage');
    }

    protected function getLoginUrl()
    {
        if ($this->session->has('mailToValide')) {
            $this->session->getFlashBag()->add('warning', "Vous n'avez pas encore validé votre compte, veuillez saisir le code envoyé dans votre mail !");
            return $this->router
                ->generate('verif_code');
        }
        return $this->router
            ->generate('security_login');
    }
}
