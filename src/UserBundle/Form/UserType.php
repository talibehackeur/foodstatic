<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Symfony\Component\Validator\Exception\ConstraintDefinitionException
     * @throws \Symfony\Component\Validator\Exception\InvalidOptionsException
     * @throws \Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('prenom')
            ->add('mail', EmailType::class)
            ->add('pseudo')
            ->add('adresse');
        if ($options["edit_admin"] == true)
            $builder->add("actif");
        else if ($options["edit"] == false)
            $builder->add('plainPassword', RepeatedType::class, array(
                'constraints' => array(
                    new NotBlank(
                        array(
                            "message" => "Ce champs ne doit pas etre vide"
                        )

                    ),
                    new Length(
                        array(
                            'max' => 4096,
                        )
                    )
                ),
                'type' => PasswordType::class,
                "invalid_message" => "Les mots de passe doivent etre identique",
                'first_options' => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Confirmez mot de passe'),
            ));

    }

    /**
     * @param OptionsResolver $resolver
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User',
            'edit_admin' => false,
            'edit' => false,
            'cascade_validation' => true,
        ));
    }
}
