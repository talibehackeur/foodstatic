<?php

namespace UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;
use UserBundle\Form\UserType;

class UsersController extends BaseController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @Route("/inscription",name="inscription")
     */
    public function inscriptionAction(Request $request)
    {
        if ($this->isAuthenticated())
            return $this->redirectToRoute('homepage');
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user
                ->setPassword($this->get('security.password_encoder')
                    ->encodePassword($user, $user->getPlainPassword()));
            $user->addRole('ROLE_CLIENT');
            // Affectation et vérification du code de validation
            $bon = false;
            while ($bon === false) {
                $x = mt_rand(1000, 9999);
                $user->setVerificationCode(mt_rand(1000, 9999));
                try {
                    $em->persist($user);
                    $em->flush();
                    $bon = true;
                } catch (\Exception $e) {
                    $bon = false;
                }
            }
            $this->addFlash("success", "Votre inscription a été bien prise en compte !");
            $this->get('session')->set("mailToValide", $user->getMail());
            $message = \Swift_Message::newInstance()
                ->setSubject('Vérification de compte')
                ->setFrom('foodstatic2017@gmail.com')
                ->setTo($user->getMail())
                ->setBody(
                    $this->renderView(
                        ':Email:valider_inscription.html.twig',
                        array(
                            'prenom' => $user->getPrenom(),
                            'nom' => $user->getNom(),
                            'villes' => $this->getVilles(),
                            "code" => $user->getVerificationCode()
                        )
                    ),
                    'text/html'
                );
            $this->get('mailer')->send($message);
            return $this->redirectToRoute("verif_code");
        }
        return $this->render('connexion/inscription.html.twig', [
            'form' => $form->createView(),
            'villes' => $this->getVilles()
        ]);
    }

    /**
     * @Route("/connexion", name="security_login")
     */
    public function loginAction()
    {
        if ($this->isAuthenticated()) {
            return $this->redirectToRoute('homepage');
        }
        $helper = $this->get('security.authentication_utils');
        return $this->render(':connexion:login.html.twig', array(
            'villes' => $this->getVilles(),
            // last username entered by the user (if any)
            'last_username' => $helper->getLastUsername(),
            // last authentication error (if any)
            'error' => $helper->getLastAuthenticationError(),
        ));
    }

    /**
     * @Route("/logout", name="logout")
     * @Security("is_authenticated()")
     */
    public function logoutAction()
    {
        $this->get('security.token_storage')->setToken(null);
        return $this->redirectToRoute("homepage");
    }

    /**
     * @Route("/login_check", name="security_login_check")
     */
    public function loginCheckAction()
    {
        // will never be executed
    }

    /**
     * @Route("/verification-code", name="verif_code")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function verifCodeAction(Request $request)
    {
        if ($this->isAuthenticated())
            return $this->redirectToRoute('homepage');
        if ($request->isMethod("POST")) {
            $mail = $this->get('session')->get("mailToValide") ?: $request->request->get('email');
            if ($mail) {
                // validation juste apres avoir ouvert un compte ou apres expiration de la session
                $code = $request->request->get("code");
                $user = $this->getDoctrine()
                    ->getRepository("UserBundle:User")
                    ->findOneBy(array("mail" => $mail));
                $em = $this->getDoctrine()->getManager();
                if (!$user) {

                    $this->addFlash('danger', 'Cet utilisateur n\'existe pas !');
                    return $this->redirectToRoute("security_login");
                } elseif ($user && !$user->isActif() && $user->getVerificationCode()) {
                    // code pas encore vérifiée
                    if ($user->getVerificationCode() === $code) {
                        $user->setActif(true);
                        $user->setVerificationCode(null);
                        $em->flush();
                        $this->addFlash("success", "Votre compte est maintenant actif, Bienvenue dans notre boutique !");
                        $this->loginUser($user);
                        $this->get("session")->remove("mailToValide");
                        return $this->redirectToRoute("homepage");
                    } else {
                        // code non valide
                        $this->addFlash("danger", "Code non valide, Veuillez vérifier le code dans votre mail");
                        return $this->redirectToRoute("verif_code");
                    }
                } elseif ($user && $user->isActif()) {
                    // l'utilisateur a déja validé son compte
                    $this->addFlash('success', 'Votre compte est déja actif, Veuillez vous connecter avec vote mail et votre mot de passe !');
                    return $this->redirectToRoute("security_login");
                }
            } else {
                $this->addFlash('warning', 'Votre session a expirée, Rempilissez ce formulaire pour valider votre compte');
                return $this->redirectToRoute("reverif_code");
            }
        }
        return $this->render(":connexion:validation_inscription.html.twig", array(
            'villes' => $this->getVilles(),
        ));
    }

    /**
     * @Route("/reverification-code", name="reverif_code")
     */
    public function verifiSessionExpireeAction()
    {
        return $this->render('connexion/validation_inscription_session_expire.html.twig', array(
            'villes' => $this->getVilles(),

        ));
    }

    /**
     * @Route("/renvoyer-code", name="renvoyer_code")
     */
    public function renvoyerCodeAction(Request $request)
    {
        if ($this->isAuthenticated()) {
            return $this->redirectToRoute('homepage');
        }
        $mail = $this->get('session')->get("mailToValide") ?: $mail = $request->request->get('mail_hidden');
        if ($mail) {
            $user = $this->getDoctrine()
                ->getRepository("UserBundle:User")
                ->findOneBy(array("mail" => $mail));
            if (!$user) {

                $this->addFlash('danger', 'Cet utilisateur n\'existe pas !');
                return $this->redirectToRoute("security_login");
            }
            if ($user && !$user->isActif() && $user->getVerificationCode()) {
                $this->addFlash("success", "Code de vérification renvoyé, Veuillez vérifier votre mail");
                $this->get("session")->set("mailToValide", $user->getMail());
                $message = \Swift_Message::newInstance()
                    ->setSubject('Vérification de comte')
                    ->setFrom('contact@rakintak.com')
                    ->setTo($user->getMail())
                    ->setBody(
                        $this->renderView(
                            ':Email:valider_inscription.html.twig',
                            array(
                                'prenom' => $user->getPrenom(),
                                'nom' => $user->getNom(),
                                "code" => $user->getVerificationCode()
                            )
                        ),
                        'text/html'
                    );
                $this->get('mailer')->send($message);
                return $this->redirectToRoute("verif_code");
            }

        } else {
            $this->addFlash("warning", "Votre session a expiré, vous devez inserer votre mail pour renvoyer la code de validation !!");
            return $this->redirectToRoute("reverif_code");
        }
    }

    /**
     * @Route("client/modifier-profil/{id}", name="modifier-profil")
     * @param Request $request
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function modifierProfilAction(Request $request, User $user)
    {
        $editForm = $this->createForm(UserType::class, $user, array(
            "edit" => true
        ));
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            $this->addFlash("success","Profil enregistrè avec succès");
            return $this->redirectToRoute('modifier-profil', array('id' => $user->getId()));
        }

        return $this->render(':connexion:modifier-profil.html.twig', array(
            'user' => $user,
            'villes' => $this->getVilles(),
            'edit_form' => $editForm->createView(),
        ));
    }


}
