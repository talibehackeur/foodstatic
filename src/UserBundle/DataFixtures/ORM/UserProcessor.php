<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 20/09/2016
 * Time: 16:05
 */

namespace UserBundle\DataFixtures\ORM;


use Nelmio\Alice\ProcessorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use UserBundle\Entity\User;

class UserProcessor implements ProcessorInterface
{
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;

    /**
     * UserProcessor constructor.
     * @param UserPasswordEncoder $passwordEncoder
     */
    public function __construct(UserPasswordEncoder $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * Processes an object before it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function preProcess($object)
    {
        if ($object instanceof User) {
            if ($object->getPseudo() === 'talibehackeur')
                $object->setRoles(array('ROLE_ADMIN'));
            else
                $object->setRoles(array('ROLE_CLIENT'));
            $object
                ->setPassword($this->passwordEncoder
                    ->encodePassword($object, $object->getPlainPassword()));
            if ($object->isActif())
                $object->setVerificationCode(null);
        }
    }

    /**
     * Processes an object after it is persisted to DB
     *
     * @param object $object instance to process
     */
    public function postProcess($object)
    {
        // TODO: Implement postProcess() method.
    }
}