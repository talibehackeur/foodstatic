<?php

namespace AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Produit;
use AppBundle\Form\ProduitType;
use AppBundle\Entity\Media;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Shared\BaseController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Produit controller.
 *
 * @Route("admin/produits")
 */
class ProduitController extends Controller
{
    /**
     * @Route("/",name="admin_list_produits")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->getRepository('AppBundle:Produit')->produits();
        $pager = $this->get("ecommerce.paginator_factory");
        $produits = $pager
            ->getPaginatedCollection($qb, $request, 36);
        return $this->render('admin/produits.html.twig', array(
            'produits' => $produits,
            'pager' => $pager->getPagerfanta()
        ));

    }


    /**
     * Modifier la photo d'un produit
     * @Route("/upload-pp/{id}",name="admin_change_pp",options={"expose"=true})
     * @Method("POST")
     * @param Request $request
     * @param Produit $produit
     * @return JsonResponse
     */
    public function sendPpAction(Request $request, Produit $produit)
    {
        $file = $request->files->get('pp');
        $media = $produit->getMedia() ?: new Media();
        $media->setFile($file);
        if ($media->isValidImage()) {
            $em = $this->getDoctrine()->getManager();
            $media->setDestination('produits');
            $media->setAlt('trigger update');
            $em->flush();
            return new JsonResponse(array(
                'data' => array(
                    'imageUrl' =>
                        "/medias/produits/" . $media->getId() . '.' . $media->getUrl(),
                )
            ), 200);

        } else {

            return new JsonResponse(array(
                'data' => array(
                    'probleme' => "Format d'image non valide"
                )
            ), 400);
        }
    }

    /**
     * Creates a new Produit entity.
     *
     * @Route("/ajouter", name="admin_produit_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function ajouterProduitAction(Request $request)
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $request->files->get("produit");
            $file = $file["file"];
            if ($file) {
                $media = New Media();
                $media->setFile($file)
                    ->setDestination("produits");
                if ($media->isValidImage()) {
                    $produit->setMedia($media);
                }
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();
            return $this->redirectToRoute('admin_list_produits');
        }

        return $this->render(':Vendeur:new.html.twig', array(
            'produit' => $produit,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Produit entity.
     *
     * @Route("/{id}/edit", name="admin_produit_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Produit $produit
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Produit $produit)
    {
        $deleteForm = $this->createDeleteForm($produit);
        $editForm = $this->createForm('AppBundle\Form\UpdateProduitType', $produit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($produit);
            $em->flush();

            return $this->redirectToRoute('admin_list_produits');
        }

        return $this->render(':Vendeur:edit.html.twig', array(
            'produit' => $produit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Produit entity.
     *
     * @Route("/{id}", name="admin_produit_delete")
     * @Method("DELETE")
     * @param Request $request
     * @param Produit $produit
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, Produit $produit)
    {
        $form = $this->createDeleteForm($produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if ($media = $produit->getMedia()) {
                $media->setDestination('produits');
            }
            $em->remove($produit);
            $em->flush();
        }

        return $this->redirectToRoute('admin_list_produits');
    }

    /**
     * Creates a form to delete a Produit entity.
     *
     * @param Produit $produit The Produit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Produit $produit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_produit_delete', array('id' => $produit->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
