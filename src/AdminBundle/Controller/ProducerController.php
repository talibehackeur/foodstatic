<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ProducerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Producer;

/**
 * Producer controller.
 *
 * @Route("/admin/producteurs")
 */
class ProducerController extends Controller
{
    /**
     * Lists all Producer entities.
     *
     * @Route("/", name="producer_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $producers = $em->getRepository('AppBundle:Producer')->findAll();

        return $this->render('producer/index.html.twig', array(
            'producers' => $producers,
        ));
    }

    /**
     * Creates a new Producer entity.
     *
     * @Route("/new", name="producer_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $producer = new Producer();
        $form = $this->createForm(ProducerType::class, $producer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($producer);
            $em->flush();

            return $this->redirectToRoute('producer_show', array('id' => $producer->getId()));
        }

        return $this->render('producer/new.html.twig', array(
            'producer' => $producer,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Producer entity.
     *
     * @Route("/{id}", name="producer_show")
     * @Method("GET")
     * @param Producer $producer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Producer $producer)
    {
        $deleteForm = $this->createDeleteForm($producer);

        return $this->render('producer/show.html.twig', array(
            'producer' => $producer,
            'produits' => $producer->getProduits(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Producer entity.
     *
     * @Route("/{id}/edit", name="producer_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Producer $producer
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Producer $producer)
    {
        $deleteForm = $this->createDeleteForm($producer);
        $editForm = $this->createForm('AppBundle\Form\ProducerType', $producer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($producer);
            $em->flush();

            return $this->redirectToRoute('producer_edit', array('id' => $producer->getId()));
        }

        return $this->render('producer/edit.html.twig', array(
            'producer' => $producer,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Producer entity.
     *
     * @Route("/{id}", name="producer_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Producer $producer)
    {
        $form = $this->createDeleteForm($producer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($producer);
            $em->flush();
        }

        return $this->redirectToRoute('producer_index');
    }

    /**
     * Creates a form to delete a Producer entity.
     *
     * @param Producer $producer The Producer entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Producer $producer)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('producer_delete', array('id' => $producer->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
