<?php

namespace AdminBundle\Controller;

use Shared\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Default controller.
 *
 * @Route("/admin")
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/",name="admin")
     */
    public function adminHomeAction()
    {
        return $this->redirectToRoute("admin_list_produits");
    }

}
