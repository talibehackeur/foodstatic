<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UpdateProduitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',null,array(
                "attr" => array(
                    "placeholder" => "Nom du produit"
                )
            ))
            ->add('prix',null,array(
                "attr" => array(
                    "placeholder" => "Prix du produit"
                )
            ))
            ->add('disponible',CheckboxType::class)
            ->add('description',null,array(
                "attr" => array(
                    "placeholder" => "Description du produit",
                    'style' => "height:300px;"
                )
            ))
            ->add('categorie',null,array(
                "placeholder" => "Choisir la catégorie du produit"
            ))
            ->add('producer',null,array(
                "placeholder" => "Choisir le producteur"
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Produit'
        ));
    }

}
