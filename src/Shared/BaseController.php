<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 18/09/2016
 * Time: 12:26
 */

namespace Shared;


use AppBundle\Entity\Pays;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\ExpressionLanguage\Expression;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use UserBundle\Entity\User;

class BaseController extends Controller
{
    /**
     * Logs this user into the system
     *
     * @param User $user
     */
    protected function loginUser(User $user)
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());

        $this->container->get('security.token_storage')->setToken($token);
    }

    /**
     * @return bool
     */
    protected function isAuthenticated()
    {
        return $this->isGranted(new Expression(
            'is_remember_me() or is_fully_authenticated()'
        ));
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager|object
     */
    protected function getDoctrineManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @return Pays[]
     */
    protected function getVilles()
    {
        return $this->getDoctrine()->getRepository("AppBundle:Pays")
            ->findAll();
    }
}