<?php
/**
 * Created by PhpStorm.
 * User: talibehackeur
 * Date: 29/07/2016
 * Time: 17:11
 */

namespace Shared\Pagination;


use AppBundle\Entity\Produit;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

class PaginationFactory
{
    private $pagerfanta;
    /**
     * @param QueryBuilder $qb
     * @param Request $request
     * @param int $maxPerPage
     * @return \AppBundle\Entity\Produit[]
     */
    public function getPaginatedCollection(QueryBuilder $qb, Request $request, $maxPerPage=12)
    {
        $page = $request->query->getInt("page", 1);
        $adapater = new DoctrineORMAdapter($qb);
        $this->pagerfanta = new Pagerfanta($adapater);
        $this->pagerfanta->setCurrentPage($page)
            ->setMaxPerPage($maxPerPage);
        $results = $this->pagerfanta->getCurrentPageResults();
        $iems = array();
        foreach ($results as $result) {
            $iems[] = $result;
        }
        return $iems;
    }

    /**
     * @return Pagerfanta
     */
    public function getPagerfanta()
    {
        return $this->pagerfanta;
    }
    

}