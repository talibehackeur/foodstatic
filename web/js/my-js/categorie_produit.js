/**
 * Created by talibehackeur on 27/09/2016.
 */
$(document).ready(function () {
    $(".ratingg").rating(
        {
            size: 'xs',
            displayOnly:true
        }
    );
});
var vm = new Vue({
    el: "#app",
    data:{
      total:0
    },
    methods: {
        ajouterAuPanier: ajouterAuPanier,
        getTotalPanier: getTotalPanier,

    }
});
vm.getTotalPanier();