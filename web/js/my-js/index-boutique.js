/**
 * Created by talibehackeur on 26/09/2016.
 */

var vm = new Vue({
    data: {
        showAside: true,
    },
    el: "#app",
    methods: {
        toggleSideBar: toggleSideBar
    }
});


$(document).ready(function () {
    $(".ma-boutique").removeClass("active");
    $("#menu-ma-boutique").addClass("active");
});