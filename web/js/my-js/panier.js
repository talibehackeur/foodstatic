/**
 * Created by talibehackeur on 28/09/2016.
 */
var vmPanier = new Vue({
    el: "#detailPanier",
    data:{
        panier:[],
        produits:[],
        total:0,
        loaded :false,
        urlHomepage : Routing.generate('homepage')
    },
    computed:{
        prixTotal:prixTotal
    },
    methods: {

        urlDetailProduit:urlDetailProduit,
        ajouterAuPanier: ajouterAuPanier,
        enleverDansPanier:enleverDansPanier,
        dimunierPanier: function (produit) {
            this.$http.post(
                Routing.generate('dimunier-panier', {'id':produit.id})).then(
                function (response) {
                    if (response.ok) {
                        this.total--;
                        vmBase.total--;
                        vmBase.panier[produit.id]--;
                        if( vmBase.panier[produit.id] === 0){
                            vmBase.produits.$remove(produit);
                            vmBase.panier.$remove(vmBase.panier[id]);
                        }
                    }
                },
                function (response) {
                    alert('Il y a eu un probléme !!');
                });
        },

    }
});
