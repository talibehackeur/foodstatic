/**
 * Created by talibehackeur on 28/09/2016.
 */
$(document).ready(function () {
    $(".menu-home").removeClass("active");
    $("#categorie").addClass("active");
});
var vm = new Vue({
    el: "#app",
    data:{
        total:0,
    },
    methods: {
        getTotalPanier: getTotalPanier,
    }
});
vm.getTotalPanier();