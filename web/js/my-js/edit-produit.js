/**
 * Created by talibehackeur on 26/09/2016.
 */

var vm = new Vue({
    data: {
        showAside: true,
        uploadingPp: false,
    },
    el: "#app",
    methods: {
        toggleSideBar: toggleSideBar,
        uploadPP: function () {
            vm.uploadingPp = true;
            var fd = new FormData(document.querySelector(".pp"));
            $.ajax({
                url: Routing.generate('admin_change_pp', {'id': event.target.id}),
                type: "POST",
                data: fd,
                processData: false,  // tell jQuery not to process the data
                contentType: false,   // tell jQuery not to set contentType
                success: function (data) {
                    if (data.data.imageUrl) {
                        $('#image_produit').attr("src", data.data.imageUrl);
                        vm.uploadingPp = false;
                    }
                },
                error: function ($error) {
                    alert(error);
                }
            });
        },

    }
});

