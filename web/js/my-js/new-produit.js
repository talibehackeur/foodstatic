/**
 * Created by talibehackeur on 26/09/2016.
 */

new Vue({
    data: {
        showAside: true
    },
    el: "#app",
    methods: {
        toggleSideBar: toggleSideBar

    }
});

$(document).ready(function () {

    $(".ma-boutique").removeClass("active");
    $("#menu-ajouter-produit").addClass("active");
    $("#input-id").fileinput({
        language: 'fr',
        showCaption: false,
        showUpload: false,
        showClose: false,
        browseLabel: 'Choisir une photo pour le produit ...'
    });
});
