/**
 * Created by talibehackeur on 28/09/2016.
 */
Vue.filter('number', function (value, decimal) {
    return parseFloat(value).toFixed(decimal);
});
var ajouterAuPanier = function (id, quantite) {
    quantite = +quantite;
    if (quantite == 0) {
        // lutilisateur na pas choisit une quantite sans la fenetre quickView
        return;
    }
    quantite = +quantite || 1;
    this.$http.post(
        Routing.generate(
            'ajouter-au-panier',
            {'id': id},
            {'quantite': quantite}
        )).then(
        function (response) {
            if (response.ok) {
                if (response.body && response.body.produit) {
                    if (!vmBase.produits) {
                        vmBase.produits = [response.body.produit]
                    } else {
                        vmBase.produits.push(response.body.produit);
                    }
                    vmBase.panier = response.body.panier;
                } else {
                    vmBase.panier[id] += quantite;
                }
                vmBase.total += quantite;
            }
        },
        function (response) {
            if (response.status === 400)
                alert("Stock insuffisant");
            else
                alert('Il y a eu un probléme !!');
        });
};
var getTotalPanier = function () {
    this.$http.get(
        Routing.generate('get-panier')).then(
        function (response) {
            if (response.ok) {
                vmBase.total = response.body.total;
                vmBase.loaded = true;
                if (typeof vmPanier !== 'undefined')
                    vmPanier.total = vmBase.total;
            }
        },
        function (response) {
            alert('Il y a eu un probléme !!');
        });
};

var urlDetailProduit = function (id) {
    return Routing.generate('produit_show', {'id': id})
};
var getProduits = function () {
    this.$http.get(
        Routing.generate('produits-panier')).then(
        function (response) {
            if (response.ok) {
                vmBase.produits = response.body.produits;
                vmBase.panier = response.body.panier;
                if (typeof vmPanier !== 'undefined') {
                    vmPanier.produits = vmBase.produits;
                    vmPanier.panier = vmBase.panier;
                    vmPanier.loaded = true;

                }
            }
        },
        function (response) {
            alert('Il y a eu un probléme !!');
        });
};
var prixTotal = function () {
    var somme = 0;
    var panier = this.panier;
    this.produits.forEach(function (produit) {
        somme = somme + produit.prix * panier[produit.id];
    });
    return somme;
};
var enleverDansPanier = function (p) {
    this.$http.post(
        Routing.generate('enlever-dans-panier', {'id': p.id})).then(
        function (response) {
            if (response.ok) {
                vmBase.total -= vmBase.panier[p.id];
                if (typeof vmPanier !== "undefined")
                    vmPanier.total -= vmBase.panier[p.id];
                vmBase.produits.$remove(p);
                if (response.body && response.body.panier) {
                    vmBase.panier = response.body.panier;
                }
            }
        },
        function (response) {
            alert('Il y a eu un probléme !!');
        });
};