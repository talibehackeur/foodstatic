var vmBase = new Vue({
    el: "#top-menu",
    data:{
        total:0,
        panier:[],
        produits:[],
        loaded : false
    },
    computed:{
        prixTotal:prixTotal
    },
    methods: {
        getTotalPanier: getTotalPanier,
        getProduits: getProduits,
        urlDetailProduit:urlDetailProduit,
        enleverDansPanier:enleverDansPanier
    }
});
vmBase.getTotalPanier();
vmBase.getProduits();
